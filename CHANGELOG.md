# Changelog

## 0.2     2020-01-28

* Fix SERIAL check not to complain about pgWikiDont internal tables

## 0.1     2020-01-28

* Initial release, checks most rules from [Pg wiki](https://wiki.postgresql.org/wiki/Don't_Do_This)
