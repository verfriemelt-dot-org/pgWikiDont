INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    SELECT array_agg( distinct query order by query ) as a
    FROM :"ttqueries"
    WHERE query ~ ' not in ?\('
)
select 'Don''t use NOT IN', 'Don.27t_use_NOT_IN', format('You have %s query/queries that use NOT IN clause:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
;
