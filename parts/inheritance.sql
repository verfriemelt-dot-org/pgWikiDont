INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    SELECT array_agg( c.oid::regclass::text ORDER BY c.oid::regclass::text ) as a
    FROM pg_catalog.pg_class c join pg_catalog.pg_inherits i on c.oid = i.inhrelid AND c.relkind = 'r'
)
select 'Don''t use table inheritance', 'Don.27t_use_table_inheritance', format('You have %s table(s) that inherit other table:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
;
