INSERT INTO :"ttwarnings" (message)
    SELECT 'Can''t check for TRUST authentication because you are not logged in as superuser.'
    WHERE NOT :'ttsuper'::bool;

INSERT INTO :"ttwarnings" (message)
    SELECT format(
        'Can''t check for TRUST authentication because your hba file (%s) is outside of data_directory (%s).',
        current_setting('hba_file'),
        current_setting('data_directory')
    ) WHERE NOT current_setting('hba_file') like format('%s/%%', current_setting('data_directory')) AND :'ttsuper'::bool;


INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    SELECT array_agg( t ) as a
    from regexp_split_to_table(pg_read_file( current_setting('hba_file') ), '[[:space:]]*[\r\n]+') as x(t)
    WHERE current_setting('hba_file') like format('%s/%%', current_setting('data_directory')) and t ~ '^[[:space:]]*host(ssl)?([[:space:]]+[^[:space:]]+){3}[[:space:]]+trust\M' AND :'ttsuper'::bool
)
select 'Don''t use trust authentication over TCP/IP (host, hostssl)', 'Don.27t_use_trust_authentication_over_TCP.2FIP_.28host.2C_hostssl.29', format('You have %s rule(s) in pg_hba.conf that use TRUST over TCP/IP:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
;
