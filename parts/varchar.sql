INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    SELECT array_agg( format( '%s.%I', c.oid::regclass::text, a.attname) ORDER BY c.oid::regclass::text, a.attname ) as a
    FROM pg_catalog.pg_attribute a join pg_catalog.pg_class c on a.attrelid = c.oid WHERE c.relkind = 'r' AND a.atttypid = 'varchar'::regtype AND a.atttypmod = -1
)
select 'Don''t use varchar(n) by default', 'Don.27t_use_varchar.28n.29_by_default', format('You have %s column(s) that use varchar datatype with no length limit:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
;
