-- Set env that makes sense for this report
\set QUIET
\set ON_ERROR_STOP 1
\timing off
\pset tuples_only 1
\pset format unaligned
\pset pager off
SET search_path = pg_catalog;

-- Start transaction so we can rollback later
BEGIN;

-- Generate random name for temp table.
select
    'pgwikidont_results_' || string_agg( chr(97 + floor(random() * 26)::int4), '') as results,
    'pgwikidont_queries_' || string_agg( chr(97 + floor(random() * 26)::int4), '') as queries,
    'pgwikidont_warnings' || string_agg( chr(97 + floor(random() * 26)::int4), '') as warnings
from generate_series(1,10) \gset tt

-- Check if current role has superuser privileges.
SELECT rolsuper as super
FROM pg_roles WHERE rolname = current_role \gset tt

-- Generate temp table for results
CREATE TEMP TABLE :"ttresults" (
    ordno serial,
    codename TEXT,
    url TEXT,
    message TEXT,
    objects TEXT[]
);

-- Generate temp table for warnings
CREATE TEMP TABLE :"ttwarnings" (
    ordno serial,
    message TEXT
);

-- Set it in GUCs, so I can use it in DO block below.
set local pgdont.queries = :'ttqueries';
set local pgdont.warnings = :'ttwarnings';

-- Make pgwikidont_queries_... view that contains list of queries, if available.
DO $$
DECLARE
    use_schema text;
    use_query TEXT;
    use_mod_query TEXT := $MOD$ trim( regexp_replace(lower(query), '[[:space:]]+', ' ', 'g')) as query $MOD$;
BEGIN
    select min(n.nspname) INTO use_schema
    from
        pg_catalog.pg_class c
        join pg_catalog.pg_namespace n on c.relnamespace = n.oid
    where
        c.relname = 'pg_stat_statements' and
        c.relkind = 'v' and
        pg_catalog.pg_get_viewdef(c.oid, true) ~ 'FROM .*pg_stat_statements\(.* pg_stat_statements\(' AND
        c.oid in (
            select a.attrelid
            from pg_catalog.pg_attribute a
            where
                a.attname in ('dbid', 'query', 'calls', 'total_time') and
                not a.attisdropped
            group by a.attrelid
            having count(distinct a.attname) = 4
        )
    ;
    IF FOUND AND use_schema IS NOT NULL THEN
        use_query = format('SELECT %s FROM %I.pg_stat_statements', use_mod_query, use_schema);
    ELSE
        use_query = 'SELECT NULL::TEXT as query WHERE false';
        execute format('INSERT INTO %I (message) VALUES ($1)', current_setting('pgdont.warnings')) USING 'You don''t seem to have pg_stat_statements enabled, so queries can''t be checked.';
    END IF;
    EXECUTE format('CREATE TEMP VIEW %I AS %s', current_setting('pgdont.queries'), use_query);
END;
$$ language 'plpgsql';

-- We don't need it anymore
RESET pgdont.queries;
RESET pgdont.warnings;

