INSERT INTO :"ttresults" (codename, url, message, objects)
WITH i as (
    SELECT array_agg( format( '%s.%I', c.oid::regclass::text, a.attname) ORDER BY c.oid::regclass::text, a.attname ) as a
    FROM pg_catalog.pg_attribute a join pg_catalog.pg_class c on a.attrelid = c.oid WHERE c.relkind = 'r' AND a.atttypid in ( 'timestamp'::regtype, 'timestamptz'::regtype ) AND a.atttypmod = 0
)
select 'Don''t use timestamp(0) OR timestamptz(0)', 'Don.27t_use_timestamp.280.29_or_timestamptz.280.29', format('You have %s column(s) that use timestamp(0) or timestamptz(0) datatype:', array_length(a, 1)), a
    FROM i
    WHERE a IS NOT NULL
;
