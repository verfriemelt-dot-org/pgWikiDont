pgWikiDont
==========

Description
-----------

pgWikiDont is an SQL test that checks if your database follows rules from
[PostgreSQL wiki](https://wiki.postgresql.org/wiki/Don%27t_Do_This).

Usage
-----
Simply change working directory to directory with checkout of pgWikiDont,
and run it via:

```
psql -f pgWikiDont.sql
```

You might need to add more options (like --username/--dbname/--host/--port).

If you need to assume a role or set some other elements, simply start psql,
set whatever you need, and then, inside of psql, run:

```
\i pgWikiDont.sql
```

