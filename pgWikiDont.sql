-- Make temporary table so other elements can store their data
\i parts/_initialize.sql

-- Run all the checks
\i parts/sqlascii.sql
\i parts/rules.sql
\i parts/inheritance.sql
\i parts/not-in.sql
\i parts/uppercase.sql
\i parts/between.sql
\i parts/timestamp.sql
\i parts/timetz.sql
\i parts/current_time.sql
\i parts/timestamp0.sql
\i parts/char.sql
\i parts/varchar.sql
\i parts/money.sql
\i parts/serial.sql
\i parts/trust.sql

-- Show report and drop temp table
\i parts/_finalize.sql
